import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App from './App';
import SnapshotTestExample from './components/snapshotTestExample1';
import SoundPlayer from '../src/components/es6mockClass/sound-player';
import SoundPlayerConsumer from '../src/components/es6mockClass/sound-player-consumer';
import CheckboxWithLabel from '../src/components/domTesting';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

it('App renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('snapshotTest example', () => {
  it('snapshot test with jest', () => {
    const component = renderer.create(<SnapshotTestExample />);
    const json = component.toJSON();
    expect(component).toMatchSnapshot(json);
  });
  // it.only('test only', () => {});
});

describe('test implementation of forEach', () => {
  function forEach(items, callback) {
    for (let index = 0; index < items.length; index++) {
      callback(items[index]);
    }
  }

  it('mock function', () => {
    const mockCallback = jest.fn(x => 42 + x);
    forEach([0, 1], mockCallback);
    expect(mockCallback.mock.calls.length).toBe(2);
  });
});

describe('es6 class mock test example', () => {
  // jest.mock('../src/components/es6mockClass/sound-player.js');

  // beforeEach(()=>{
  //   SoundPlayer.mockClear();
  // });

  // it('check if consumer called the mock construcotr', ()=>{
  //   const soundPlayerConsumer = new SoundPlayerConsumer();
  //   console.log(SoundPlayer.toHaveBeenCalled(1));
  // });
});

describe('dom testing', () => {
  const checkbox = shallow(<CheckboxWithLabel labelOn="on" labelOff="off" />);
  expect(checkbox.text()).toEqual('off');
});

console.log(jest);